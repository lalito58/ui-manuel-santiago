import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './ui-manuel-santiago-styles.js';
import '@bbva-web-components/bbva-list-card';
/**
This component ...

Example:

```html
<ui-manuel-santiago></ui-manuel-santiago>
```

##styling-doc

@customElement ui-manuel-santiago
@polymer
@LitElement
@demo demo/index.html
*/
export class UiManuelSantiago extends LitElement {
  static get is() {
    return 'ui-manuel-santiago';
  }

  // Declare properties
  static get properties() {
    return {
      productList: { type: Array }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.productList = [];
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('ui-manuel-santiago-shared-styles')
    ];
  }

  // Define a template
  render() {
    return html`
      ${this.productList.map(item => html`
        <bbva-list-card
          type="card"
          card-title="${item.title.name}"
          card-image="${item.images[0].url}"
          currency-code="${item.currencies[0].currency}"
          amount="${item.availableBalance.currentBalances[0].amount}"
          num-product="${item.cardId}"
          @list-card-click="${ this._handleCardClick }"
          .item="${item}">
        </bbva-list-card>
      `)}
    `;
  }
  _handleCardClick(evt) {
    const detail = evt.target.item || {};
    this.dispatchEvent(new CustomEvent('product-clicked', {
      bubbles: true,
      composed: true,
      detail: detail
    }));
  }
}

// Register the element with the browser
customElements.define(UiManuelSantiago.is, UiManuelSantiago);
